const path = require('path');

module.exports = {
  transpileDependencies: ['vuex-module-decorators'],
  publicPath: process.env.NODE_ENV === 'production'
    ? '/<project-name>/'
    : '/',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'raw-loader',
            query: {
              name: 'src/assets/images/icon/[name].[ext]'
            }
          },
        ]
      }
    ]
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src/'),

    },
  },

}