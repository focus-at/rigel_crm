export default {
  data() {
    return {
      filters: {
        search: {
          key: "name",
          value: null,
        },
      },
      options: {
        pages: 1,
        limit: 10,
      },
      columns: [

        {
          header: 'Дата',
          field: 'code',
        },
        {
          header: 'Обновлено',
          field: 'code',
        },
        {
          header: 'Артикуль',
          field: 'code',
        },
        {
          header: 'Размер',
          field: 'code',
        },
        {
          header: 'Баркод',
          field: 'code',
        },
        {
          header: 'Остатки',
          field: 'code',
        },
        {
          header: 'Цена',
          field: 'code',
        },
        {
          header: 'Скидка',
          field: 'code',
        },
        {
          header: 'Номер поставки',
          field: 'code',
        },
        {
          header: 'Бренд',
          field: 'code',
        },
      ],
      searchByField: [
        {
          name: "Location name",
          value: "name",
        },
        {
          name: "Location address",
          value: "address",
        },
        {
          name: "Station ID",
          value: "stationId",
        },
        {
          name: "Station MC",
          value: "stationManualCode",
        },
        {
          name: "Station SN",
          value: "stationSerialNumber",
        },
      ],
    }
  }
}