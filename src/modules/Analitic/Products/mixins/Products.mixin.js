export default {
  data() {
    return {
      filters: {
        search: {
          key: "name",
          value: null,
        },
      },
      options: {
        pages: 1,
        limit: 10,
      },
      searchByField: [
        {
          name: "Location name",
          value: "name",
        },
      ],

      columns: [

        {
          header: 'Фото',
          field: 'code',
          frozen: true
        },
        {
          header: 'SKU',
          field: 'code',
          frozen: true
        },
        {
          header: 'Название',
          field: 'code',
          frozen: true
        },
        {
          header: 'Категория',
          field: 'code',
          frozen: true
        },
        {
          header: 'Позиция',
          field: 'code',
          frozen: false
        },
        {
          header: 'Кол-во',
          field: 'code',
          frozen: false
        },
        {
          header: 'Бренд',
          field: 'code',
          frozen: false
        },
        {
          header: 'Продавец',
          field: 'code',
          frozen: false
        },
        {
          header: 'Наличие',
          field: 'code',
          frozen: false
        },
        {
          header: 'Комментариев',
          field: 'code',
          frozen: false
        },
        {
          header: 'Рейтинг',
          field: 'code',
          frozen: false
        },
        {
          header: 'Цена',
          field: 'code',
          frozen: false
        },
        {
          header: 'Цена СПП',
          field: 'code',
          frozen: false
        },
        {
          header: 'Дней в наличии',
          field: 'code',
          frozen: false
        },
        {
          header: 'Продаж',
          field: 'code',
          frozen: false
        },
        {
          header: 'Выручка',
          field: 'code',
          frozen: false
        },

      ],
    }
  }
}