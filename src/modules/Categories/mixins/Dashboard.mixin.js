export default {
  data() {
    return {
      filters: {
        search: {
          key: "name",
          value: null,
        },
      },
      options: {
        pages: 1,
        limit: 10,
      },
      searchByField: [
        {
          name: "Location name",
          value: "name",
        },
        {
          name: "Location address",
          value: "address",
        },
        {
          name: "Station ID",
          value: "stationId",
        },
        {
          name: "Station MC",
          value: "stationManualCode",
        },
        {
          name: "Station SN",
          value: "stationSerialNumber",
        },
      ],
    }
  }
}