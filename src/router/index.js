import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'LoginLayout',
    component: () => import('@/Layouts/Auth.vue'),
    children: [
      {
        path: '',
        name: 'Login',
        component: () => import('@/views/Auth/Login.vue'),

      }
    ]
  },
  {
    path: '/dashboard',
    name: 'DashboardLayout',
    component: () => import('@/Layouts/Dashboard.vue'),
    children: [
      {
        path: '/dashboard/login',
        name: 'Login',
        meta: {
          pageTitle: 'Войти в систему',
          icon: "pi-home",
        },
        component: () => import('@/modules/Login/views/Index.vue'),
      },
      {
        path: '/dashboard/main',
        name: 'Dashboard',
        meta: {
          pageTitle: 'Рабочий стол',
          icon: "pi-home",
        },
        component: () => import('@/modules/Dashboard/views/Index.vue'),
      },
      {
        path: '/dashboard/analitic',
        name: 'Analitic',
        meta: {
          pageTitle: 'Анализ рынка',
          icon: "pi-home",
        },
        children: [
          {
            path: '/dashboard/analitic/products',
            name: 'Products',
            meta: {
              pageTitle: 'Товары',
            },
            component: () => import('@/modules/Analitic/Products/views/Index.vue'),
          },
          {
            path: '/dashboard/analitic/category',
            name: 'Category',
            meta: {
              pageTitle: 'Категории',
            },
            component: () => import('@/modules/Analitic/Category/views/Index.vue'),
          },
          {
            path: '/dashboard/analitic/brand',
            name: 'Brand',
            meta: {
              pageTitle: 'Бренды',
            },
            component: () => import('@/modules/Analitic/Brand/views/Index.vue'),
          },
          {
            path: '/dashboard/analitic/salers',
            name: 'Salers',
            meta: {
              pageTitle: 'Продавцы',
            },
            component: () => import('@/modules/Analitic/Salers/views/Index.vue'),
          },
          {
            path: '/dashboard/analitic/trand',
            name: 'Trand',
            meta: {
              pageTitle: 'Тренд',
            },
            component: () => import('@/modules/Dashboard/views/Index.vue'),
          },
          {
            path: '/dashboard/analitic/days',
            name: 'Days',
            meta: {
              pageTitle: 'По дням',
            },
            component: () => import('@/modules/Dashboard/views/Index.vue'),
          },
          {
            path: '/dashboard/analitic/segmentation',
            name: 'Segmentation',
            meta: {
              pageTitle: 'Ценовая сегментация',
            },
            component: () => import('@/modules/Analitic/Products/views/Index.vue'),
          },
          {
            path: '/dashboard/analitic/compare',
            name: 'Compare',
            meta: {
              pageTitle: 'Сравнение периодов',
            },
            component: () => import('@/modules/Analitic/Products/views/Index.vue'),
          },
          {
            path: '/dashboard/analitic/items',
            name: 'Items',
            meta: {
              pageTitle: 'Предметы',
            },
            component: () => import('@/modules/Analitic/Products/views/Index.vue'),
          },
        ],
      },
      {
        path: '/dashboard/categories',
        name: 'Categories',
        meta: {
          pageTitle: 'Категории',
          icon: "pi-home",
        },
        component: () => import('@/modules/Categories/views/Index.vue'),
      },
      {
        path: '/dashboard/orders',
        name: 'Orders',
        meta: {
          pageTitle: 'Мои заказы',
          icon: "pi-home",
        },
        component: () => import('@/modules/MyOrders/views/Index.vue'),
      },
      {
        path: '/dashboard/orders/:id',
        name: 'OrdersView',
        meta: {
          pageTitle: '1222',
          icon: "pi-home",
        },
        component: () => import('@/modules/MyOrders/views/View.vue'),
      },
      {
        path: '/dashboard/sales',
        name: 'Sales',
        meta: {
          pageTitle: 'Мои продажи',
          icon: "pi-home",
        },
        component: () => import('@/modules/Dashboard/views/Index.vue'),
      },

    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
