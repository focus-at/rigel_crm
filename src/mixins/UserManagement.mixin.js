export default {
    data() {
        return {
            filters: {
                search: {
                    key: 'name',
                    value: null,
                },
                date: {
                    key: 'date',
                    value: null,
                },
                role: {
                    key: 'role',
                    value: null,
                },
            },
            searchByField: [

                {
                    text: 'Name',
                    value: 'name',
                },
                {
                    text: 'Email',
                    value: 'email',
                },

            ],
            options: {
                pages: 1,
                limit: 10,
            },
            sort: {},
            columns: [
                {
                    header: 'Name',
                    field: 'name',
                    sortable: true,
                },
                {
                    header: 'Role',
                    field: 'roles',
                    sortable: false,
                },
                {
                    header: 'Email',
                    field: 'email',
                    sortable: true,
                },
                {
                    header: 'Phone number',
                    field: 'data.phone',
                    sortable: true,
                },
                {
                    header: 'Invested $',
                    field: 'data.invested',
                    sortable: false,
                },
                {
                    header: 'Income',
                    field: 'statistics.income',
                    sortFieldName: 'statisticsValue',
                    filter: {
                        filterKey: 'statisticsKey',
                        filterValue: 'income'
                    },
                    sortable: true,
                },
                {
                    header: 'Max № of pulled power banks',
                    field: 'powerbanksEjectLimit',
                    sortable: false,
                },
                {
                    header: 'Pulled power banks',
                    field: 'ejectedPowerbanks',
                    sortable: false,
                },
                {
                    header: 'Stations',
                    field: 'stations',
                    sortable: false,
                    sortable: false,
                },
                {
                    header: 'Contracts',
                    field: 'contracts',
                    sortable: false,
                },
                {
                    header: 'Comments',
                    field: 'history',
                    sortable: false,
                },

            ],
            columns_history: [
                {
                    header: "User Name",
                    field: "userName",
                },
                {
                    header: "User email",
                    field: "userEmail",
                },
                {
                    header: "Comments",
                    field: "comments",
                },
                {
                    header: "Date",
                    field: "timestamp",
                },
            ],
        }
    },
    methods: {
        sortTable(event) {
            let sortField = event.sortField
            let f = _.find(this.columns, (item) => {
                return item.field == event.sortField
            })
            if (f.hasOwnProperty('sortFieldName')) {
                sortField = f.sortFieldName
            }
            if (f.hasOwnProperty('filter')) {
                this.sort.filter = f.filter
            }
            this.sort.key = sortField;
            this.sort.value = event.sortOrder == 1 ? "ASC" : "DESC";
            this.getData();
        },
    }
}