let apiURL = process.env.VUE_APP_API_DEVELOP_URL
const ENV = process.env.VUE_APP_ENVAIRMENT

if (window.location.hostname === 'crm-new.fastenergy.world') {
  apiURL = process.env.VUE_APP_API_PROD_URL
}

const config = function (cook) {
  return {
    baseURL: apiURL,
    headers: {
      Accept: 'application/json',
      // Token: `${cook}`
    },
  };
};

export default config;
