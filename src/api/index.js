import axios from 'axios';
import config from './config';

import _ from 'lodash'

const lifeTime = 3000;

import Cookies from 'js-cookie'

export const customAxios = axios.create(config(Cookies.get('token')));
customAxios.interceptors.request.use(function (config) {
    customAxios.defaults.params = { nocache: +new Date() }

    config.headers.Token = Cookies.get('token');
    // config.headers.Accept = 'application/json'

    return config;
}, null, { synchronous: true });
// 
customAxios.interceptors.response.use(
    response => response.data.data,
    error => {
        // console.log(_.head(error.response.data.data));
        if (error.response.status === 401) {
            alert('Please login before use crm.')
            Cookies.remove('token')
            window.location.href = '/'
        }

        if (error.response.status === 405) {
            alert('Wrong methods.')
            Cookies.remove('token')
            window.location.href = '/'
        }

        if (error.response.status === 400) {
            var x = document.getElementById("snackbar");
            x.className = "show";
            x.innerText = error.response.data.data[0];

            setTimeout(function () { x.className = x.className.replace("show", ""); }, lifeTime);

            return false

            $toast.error(
                errorTranslations[error.response.data.data[0]] ||
                error.response.data.data[0] ||
                'Sorry, an error occured, please try again later.'
            )

            return false
        }

        return error.response.data
    });

export default customAxios;