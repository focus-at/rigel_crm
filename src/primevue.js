import Accordion from 'primevue/accordion';
import AccordionTab from 'primevue/accordiontab';
import Avatar from 'primevue/avatar';
import AvatarGroup from 'primevue/avatargroup';
import BadgeDirective from 'primevue/badgedirective';
import Button from 'primevue/button';
import Calendar from 'primevue/calendar';
import Card from 'primevue/card';
import Carousel from 'primevue/carousel';
import Chart from 'primevue/chart';
import Checkbox from 'primevue/checkbox';
import Chip from 'primevue/chip';
import Column from 'primevue/column';
import DataTable from 'primevue/datatable';
import Dialog from 'primevue/dialog';
import Divider from 'primevue/divider';
import Dropdown from 'primevue/dropdown';
import FileUpload from 'primevue/fileupload';
import InputNumber from 'primevue/inputnumber';
import InputSwitch from 'primevue/inputswitch';
import InputText from 'primevue/inputtext';
import Knob from 'primevue/knob';
import Menu from 'primevue/menu';
import MultiSelect from 'primevue/multiselect';
import Paginator from 'primevue/paginator';
import ProgressBar from 'primevue/progressbar';
import RadioButton from 'primevue/radiobutton';
import Rating from 'primevue/rating';
import Ripple from 'primevue/ripple';
import Sidebar from 'primevue/sidebar';
import Slider from 'primevue/slider';
import StyleClass from 'primevue/styleclass';
import TabMenu from 'primevue/tabmenu';
import TabPanel from 'primevue/tabpanel';
import TabView from 'primevue/tabview';
import Tag from 'primevue/tag';
import Textarea from 'primevue/textarea';
import Toast from 'primevue/toast';
import ToastService from 'primevue/toastservice';
import Tooltip from 'primevue/tooltip';
import SelectButton from 'primevue/selectbutton';

import ConfirmationService from 'primevue/confirmationservice';
import ConfirmDialog from 'primevue/confirmdialog';

import ColumnGroup from 'primevue/columngroup';     //optional for column grouping
import Row from 'primevue/row';                     //optional for row
import SplitButton from 'primevue/splitbutton';
import SpeedDial from 'primevue/speeddial';
import ContextMenu from 'primevue/contextmenu';
import Panel from 'primevue/panel';
import DialogService from 'primevue/dialogservice';
import DynamicDialog from 'primevue/dynamicdialog';
import BlockUI from 'primevue/blockui';
import Image from 'primevue/image';
import ProgressSpinner from 'primevue/progressspinner';
import TieredMenu from 'primevue/tieredmenu';
import Menubar from 'primevue/menubar';

export default {
  install(app) {
    // Services
    app.use(ToastService);
    app.use(ConfirmationService);
    app.use(DialogService);

    // Directives
    app.directive('badge', BadgeDirective);
    app.directive('tooltip', Tooltip);
    app.directive('ripple', Ripple);
    app.directive('styleclass', StyleClass);

    // Components
    app.component('TieredMenu', TieredMenu);
    app.component('Menubar', Menubar);
    app.component('Image', Image);
    app.component('ProgressSpinner', ProgressSpinner);
    app.component('Panel', Panel);
    app.component('BlockUI', BlockUI);
    app.component('Accordion', Accordion);
    app.component('SplitButton', SplitButton);
    app.component('ContextMenu', ContextMenu);
    app.component('SpeedDial', SpeedDial);
    app.component('ConfirmDialog', ConfirmDialog);
    app.component('DynamicDialog', DynamicDialog);
    app.component('SelectButton', SelectButton);
    app.component('ColumnGroup', ColumnGroup);
    app.component('Row', Row);
    app.component('AccordionTab', AccordionTab);
    app.component('Avatar', Avatar);
    app.component('AvatarGroup', AvatarGroup);
    app.component('Button', Button);
    app.component('Calendar', Calendar);
    app.component('Card', Card);
    app.component('Chart', Chart);
    app.component('Carousel', Carousel);
    app.component('Checkbox', Checkbox);
    app.component('Chip', Chip);
    app.component('Dialog', Dialog);
    app.component('Column', Column);
    app.component('Slider', Slider);
    app.component('MultiSelect', MultiSelect);
    app.component('DataTable', DataTable);
    app.component('Divider', Divider);
    app.component('Dropdown', Dropdown);
    app.component('FileUpload', FileUpload);
    app.component('InputNumber', InputNumber);
    app.component('InputSwitch', InputSwitch);
    app.component('InputText', InputText);
    app.component('Knob', Knob);
    app.component('Menu', Menu);
    app.component('ProgressBar', ProgressBar);
    app.component('RadioButton', RadioButton);
    app.component('Rating', Rating);
    app.component('Sidebar', Sidebar);
    app.component('TabMenu', TabMenu);
    app.component('TabPanel', TabPanel);
    app.component('TabView', TabView);
    app.component('Tag', Tag);
    app.component('Textarea', Textarea);
    app.component('Toast', Toast);
    app.component('Paginator', Paginator);
  }
}