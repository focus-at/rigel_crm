import moment from 'moment'
import prettyMs from 'pretty-ms'

export default {
    unixToLocaleFullTime: (unixTime) => {
        if (!unixTime) return null

        const date = new Date(unixTime * 1000)
        const formattedDate = date.toLocaleDateString()
        const formattedTime = date.toLocaleTimeString()

        return `${formattedDate} ${formattedTime}`
    },

    toBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            // Sets up even listeners BEFORE you call reader.readAsDataURL
            reader.onload = function () {
                const result = reader.result
                return resolve(result);
            };

            reader.onerror = function (error) {
                return reject(error);
            };
            // Calls reader function
            reader.readAsDataURL(file);
        })
    },
    generatedPassword(strengthLevel = 12) {
        let charactersArray = 'a-z'.split(',')
        let CharacterSet = ''
        let password = ''
        let size = 8

        switch (strengthLevel) {
            case 12:
                size = 10
                charactersArray = 'a-z,A-Z'.split(',')
                break
            case 24:
                size = 12
                charactersArray = 'a-z,A-Z,0-9'.split(',')
                break
            case 36:
                size = 14
                charactersArray = 'a-z,A-Z,0-9,#'.split(',')
                break
            case 48:
                size = 16
                charactersArray = 'a-z,A-Z,0-9,#'.split(',')
                break
        }
        if (charactersArray.indexOf('a-z') >= 0) {
            CharacterSet += 'abcdefghijklmnopqrstuvwxyz'
        }
        if (charactersArray.indexOf('A-Z') >= 0) {
            CharacterSet += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        }

        if (charactersArray.indexOf('0-9') >= 0) {
            CharacterSet += '0123456789'
        }
        if (charactersArray.indexOf('#') >= 0) {
            CharacterSet += '![]{}()%&*$#^<>~@|'
        }
        for (let i = 0; i < size; i++) {
            password += CharacterSet.charAt(Math.floor(Math.random() * CharacterSet.length))
        }

        return password
    },
    dateFormat: (timestamp, type = "") => {
        if (!timestamp) {
            return '-'
        }
        if (type == "withMinute") {
            return moment.unix(timestamp).format("DD/MM/YYYY HH:m")
        }
        if (type == "withSecondMinute") {
            return moment.unix(timestamp).format("DD/MM/YYYY HH:m:s")
        }
        return moment.unix(timestamp).format("DD/MM/YYYY")
    },
    calculateTimeToNow: (timestamp) => {
        if (timestamp == 0) return 0
        return prettyMs(new Date() - timestamp * 1000, {
            secondsDecimalDigits: 0,
            millisecondsDecimalDigits: 0,
        })
    },
    unixToLocaleHours: (unixTime) => {
        if (!unixTime) return null
        const days = unixTime / 60 / 60 / 24
        const hours = (days % 1) * 24
        const minutes = (hours % 1) * 60
        return `${Math.floor(days)}d ${Math.floor(hours)}h ${Math.floor(minutes)}m`
    },
    getStatusStyle: (status, type = 1) => {
        if (type == 1) {
            return status == 'offline' ? 'danger' : 'success'
        }

        if (type == 2) {
            return status == 'active' ? 'success' : 'danger'
        }
    },
    getStatusPaybackStyle: (status) => {
        if (status == 0) {
            return 'danger'
        } else if (status <= 50) {
            return 'warning'
        } else {
            return 'success'
        }
    },
    generateFilterString(filters = {}, options = {}, sort = {}) {
        const params = {
            page: options.page,
            limit: options.limit,
        }
        if (sort.key) {
            params[`sort[${sort.key}]`] = sort.value
            if (sort.hasOwnProperty('filter')) {
                params[`filter[${sort.filter.filterKey}]`] = sort.filter.filterValue
            }
        }
        for (let filter in filters) {

            const filterKey = filters[filter].key
            const filterValue = filters[filter].value

            if (
                filterValue !== null &&
                `${filterValue}`.length !== 0
            ) {

                if (typeof filterKey === 'string') {
                    params[`filter[${filterKey}]`] = filterValue
                } else {
                    params[`filter[${filterKey.key}]`] = filterKey.value
                    params[`filter[${filterKey.filterBy}]`] = filterValue
                }

                if (filters.hasOwnProperty('date')) {

                    if (filters.date.value !== null) {
                        if (filters.date.value[0]) {
                            params['filter[dateStart]'] = moment(filters.date.value[0]).format("YYYY-MM-DD")
                        }

                        if (filters.date.value[1]) {
                            params['filter[dateStart]'] = moment(filters.date.value[1]).format("YYYY-MM-DD")
                        }
                    }
                }
            }
        }
        // delete filter[date]
        // console.log(params);
        return params
    }
}