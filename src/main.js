import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'


const app = createApp(App);
// import registerModules from "./register-modules";

// import SplitPayments from "./modules/Payments/SplitPayments";
// registerModules({
//     SplitPayments: SplitPayments,
// });

import vueKanban from 'vue-kanban'

app.use(vueKanban)

import PrimeVue from 'primevue/config';
app.use(PrimeVue, { ripple: true, theme: 'lara-light-indigo', dark: false });

import PrimeVueComponents from './primevue'
app.use(PrimeVueComponents);

import PageTitle from "@/Layouts/components/PageTitle";
app.component('PageTitle', PageTitle);

// import MainInput from "@/components/common/MainInput";
// app.component('FsMainInput', MainInput);


import GF from "./globalFunction"
let globalFunction = GF
// app.provide('gbUtilities', globalFunction)
app.config.globalProperties.$gbUtilities = globalFunction

// import QrcodeVue from 'qrcode.vue'
// app.component(QrcodeVue);

app.use(store);
app.use(router);
app.mount('#app')
